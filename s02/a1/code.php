<?php 
	$product = []; //global variable
	function createProduct($productName,$price){
		global $product; //we are telling the createProduct() that w aare going to use a global variable called $product and modify it n its block

		// solution
		// array_push($product, ['name' => $productName,'price' =>$price])

		 array_push($product,
		["productName" => "$productName",
			"price" => "$price"
		]);
	}

	// solution
	// function printProduct(){
	// 	global $product;
	// 	foreach ($product as $products) {
	// 		# code...
	// 		echo "<li>";
	// 		foreach ($products as $key => $value) {
	// 			# code...
	// 			echo "$key : $value";
	// 		}
	// 		echo "</li>";
	// 	}
	// }
	function printProducts($productList){
		// global $product;
		// print_r($productList);
		foreach ($productList as $key => $prod) {
			# code...
			foreach ($prod as $key =>$values) {
				# code...
				echo "<li><strong>$key:</strong>&nbsp; $values</li>";
			}
		}
	}
	// solution
	// function countProducts(){
	// 	global $product;
	// 	echo count($product);
	// }
	function countPrducts($productCount){
		$arrProductLength = count($productCount);
		echo "<li>$arrProductLength</li>";
	}

	function deleteProduct(){
		global $product;
		array_pop($product);	
	}
 ?>