<?php 
	
	$buildingObj = (object)[
		'name' => 'Caswynn Bldg',
		'floors' => 8,
		'address' => (object)[
			'barangay' => 'Sacred Heart',
			'city' => 'Quezon City',
			'country' => 'Philippines'
		]
	];


	class Building { //creating a blueprint/base class of our Building
		// public keyword - visibility level of how the methods/properties can bee accessed by the outsiders
		//public, private, protected
		//declaration of properties of a building
		public $name; //publicly, the $names,$floors and $address are visible to the outsiders
		public $floors;
		public $address;

		//Constructor Functions - special type of functions which is called automatically whenever we a instantiating an object(create an instgances of a class)

		public function __construct($nameValue,$floorsValue,$addressValue){
			//$this keyword is a reserved keyword in PHP that refers to the class itself.
			$this->name = $nameValue;
			$this->floors = $floorsValue;
			$this->address = $addressValue;
		}
	}
 ?>